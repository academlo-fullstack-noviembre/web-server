//Escribir/Sobreescribir sobre un archivo
fs.writeFile("saludo.txt", "Hello world", (error) => {
    if(error){
        console.log("No se pudo escribir sobre el archivo");
    }else{
        console.log("Se ha escrito en el archivo");
    }
});

//Adjuntar
fs.appendFile("saludo.txt", " Hola mundo", (error) => {
    if(error){
        console.log("No se pudo escribir sobre el archivo");
    }else{
        console.log("Se ha escrito en el archivo");
    }
});

fs.readFile("saludo.txt", (err, datos) => {
    if(err){
        console.log("No se pudo leer el archivo");
        console.log(err);
    }else{
        console.log(datos.toString());
    }
});
