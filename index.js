let http = require("http");
let fs = require("fs");

http.createServer((request, response) => {
    //Crear nuestro servidor web
    console.log(request.url);
    switch(request.url){
        case "/merch": 
            //Mandamos una respuesta con la mercancia que maneja academlo
            response.setHeader("Content-Type", "text/html");            
            response.writeHead(200);
            response.write("<h1>Esta es la mercancia que maneja Academlo</h1>");
            response.end(); //terminamos de escribir la respuesta al cliente
            break;
        case "/playeras": 
            //Mandamos un saludo
            response.write("Bienvenido a nuestra tienda en linea");
            response.end(); //terminamos de escribir la respuesta al cliente
            break;
        case "/contacto":
            fs.readFile("./contacto.html", (error, resultado) => {
                if(error){
                    response.write("<h1>No se encuentra el recurso solicitado</h1>");                    
                }else{
                    response.write(resultado);
                }
                response.end();
            });
            break;
        default: 
            getFiles(request.url, response);
            break;
    }
    //Listen para escuchar las peticiones que se hacen sobre x puerto
}).listen(8000);

const getFiles = (url, response) => {
    let path = __dirname; //__dirname es una variable global para obtener la ruta absoluta
    fs.readFile(path + url, (error, resultado) => {
        //concatenamos la ruta absoluta con el recurso solicitado por el cliente
        if(error){
            //En caso de que no se encuentre el archivo
            response.writeHead(404);
            response.write("<h1>No se encuentra el recurso solicitado</h1>");                    
        }else{
            //en caso de que si se encuentre el archivo
            //mandaremos su contenido
            response.write(resultado);
        }
        response.end();
    });
}
